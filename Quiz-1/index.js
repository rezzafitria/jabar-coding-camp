// soal 1
function next_date(tanggal, bulan, tahun) {
    var format = {year: 'numeric', month: 'long', day: 'numeric' };
    const tglBaru = new Date(tahun, bulan-1, tanggal+1);
    console.log(tglBaru.toLocaleDateString("id", format));
}
var tanggal = 29;
var bulan = 2;
var tahun = 2020;
console.log(next_date(tanggal, bulan, tahun));

// soal 2
function jumlah_kata(str) { 
    return str.split(" ").length;

}
var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok";
var kalimat_2 = "Saya Iqbal";
console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));