//soal 1
var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'];
var output = [];
var inserted;

for (var i = 0, ii = daftarHewan.length ; i < ii ; i++){
  inserted = false;
  for (var j = 0, jj = output.length ; j < jj ; j++){
    if (daftarHewan[i] < output[j]){
      inserted = true;
      output.splice(j, 0, daftarHewan[i]);
      break;
    }
  }
  
  if (!inserted)
    output.push(daftarHewan[i])
}
console.log(output);

// soal 2
function introduce(name, age, address, hobby) {
    this.name = name;
    this.age = age;
    this.address = address;
    this.hobby = hobby;
}
var data = new introduce("Rezza", 23, "Kab Bandung", "Nonton");
var perkenalan = introduce(data);
console.log("Nama Saya " +data.name, "Umur Saya "+ data.age +" tahun", "Alamat saya di " +data.address, "dan saya punya hobby yaitu "+data.hobby);

// soal 3
function hitung_huruf_vokal(str1)
{
  var vowel_list = 'aeiouAEIOU';
  var vcount = 0;
  
  for(var x = 0; x < str1.length ; x++)
  {
    if (vowel_list.indexOf(str1[x]) !== -1)
    {
      vcount += 1;
    }
  
  }
  return vcount;
}
var hitung_1 = "Muhammad";
var hitung_2 = "Iqbal";
console.log(hitung_huruf_vokal(hitung_1));
console.log(hitung_huruf_vokal(hitung_2));

//soal 4
function hitung() {
    this.hitung0 = -2;
    this.hitung1 = 0;
    this.hitung2 = 2;
    this.hitung3 = 4;
    this.hitung5 = 8;
}
    var coba1 = new hitung(+2);
    var coba2 = hitung(coba1);
    console.log(hitung0);
    console.log(hitung1);
    console.log(hitung2);
    console.log(hitung3);
    console.log(hitung5);