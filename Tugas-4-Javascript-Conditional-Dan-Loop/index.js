//soal 1
var nilai = 97;
if (nilai >= 85) {
    console.log("indeks A")
} else if (nilai >= 77 && nilai <= 84){
    console.log("indeks B")
} else if (nilai >= 65 && nilai <= 74){
    console.log("indeks C")
} else if (nilai >= 55 && nilai <= 64){
    console.log("indeks D")
} else {
    console.log("indeks E")
}
// soal 2
var tanggal = 18;
var tahun = 1997;
var strtanggal = String(tanggal);
var strtahun = String(tahun);
var bulan = 12;
switch(bulan) {
    case 1: 
        bulan = 'JANUARI';
        break;
    case 2: 
        bulan = 'FEB';
    case 3: 
        bulan = 'MAR';
        break;
    case 4: 
        bulan = 'APR';
    case 5: 
        bulan = 'MEI';
    case 6: 
        bulan = 'JUN';
        break;
    case 7: 
        bulan = 'JUL';
    case 8: 
        bulan = 'AGS';
        break;
    case 9: 
        bulan = 'SEPT';
    case 10: 
        bulan = 'OKT';
        break;
    case 11: 
        bulan = 'NOV';    
        break;
    case 12: 
        bulan = ' DESEMBER ';
        break;
}
console.log(strtanggal.concat(bulan, strtahun));


// soal 3
function segitiga1(tinggi) {
    var n = 3;
    for (let i = 0; i < tinggi; i++) {
        for (let j = 0; j <= i; j++) {
            n += '# ';
        }
        n += '\n';
    }
    return n;
}
console.log(segitiga1(3));


// soal 4
const kalimat = ["Programming", "Javascript", "VueJS"];
let panjang = kalimat.length;
let index = 0;
let nomor = 1;
var m = 10;
while (nomor <= m) {
    console.log(nomor+" - I love "+kalimat[index]);
    index+=1;
    nomor+=1;
    if (index == panjang) {
        console.log("=".repeat(nomor-1));
        index = 0;
    }
}