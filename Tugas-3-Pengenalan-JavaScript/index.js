// soal 1
var pertama = "saya sangat senang hari ini"
var per1 = pertama.substring(0, 5);
var per2 = pertama.substring(12, 19);
var kedua = "belajar javascript itu keren"
var ke2 = kedua.substring(0,8);
var ke3 = kedua.substring(8, 19);
var upper = ke3.toUpperCase();

console.log(per1.concat(per2, ke2, upper));

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var strkataPertama = parseInt(kataPertama);
var strkataKedua = parseInt(kataKedua);
var strkataKetiga = parseInt(kataKetiga);
var strkataKeempat = parseInt(kataKeempat);

console.log((strkataPertama-strkataKeempat)*(strkataKedua+strkataKetiga));

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(14, 18);
var kataKeempat = kalimat.substring(18, 24); 
var kataKelima = kalimat.substring(24, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);